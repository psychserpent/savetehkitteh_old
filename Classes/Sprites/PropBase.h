#ifndef __PROPBASE_H__
#define __PROPBASE_H__
/*
Prop Sprite
------------------------------------------------------------------
Props are objects that are often manipulated by physics - some can
collide with the kitteh, while some don't.

Notes:
	Revising this as of February something of something day...
*/
#include "cocos2d.h"
#include "PropHazard.h"
#include "PropAttachPoint.h"
#include "Macros.h"

USING_NS_CC;	// Cocos2d Namespace

class PropBase : public CCSprite
{
private:
	CCPoint _gravity;
    CCArray* _attachPoints;
    CCArray* _hazardArray;
    // Methods
    const CCPoint& getDirection(const CCPoint& target);
public:
	// Constructors
    //-------------------------------------------------------------------------
    PropBase(void);
    ~PropBase(void);
    
	CREATE_FUNC(PropBase);
    CREATE_FUNC_WITH_GRAVITY(PropBase);
    
	// Properties
    //-------------------------------------------------------------------------
	const CCPoint& getGravity();
    void setGravity(const CCPoint& gravity);

    CCArray* getAttachPoints();
    void addAttachPoint(PropAttachPoint* attachPoint);

    CCArray* getHazardArray();
    void setHazardArray(CCArray* arr);

    bool hasAttached();

	// Methods
    //-------------------------------------------------------------------------
	virtual bool init();
    virtual bool initWithGravity(const CCPoint& gravity);

	void bump(CCNode* collider);
    void loose(CCNode* sender, void* data);
	bool tryAttach(PropHazard* hazard);
	
    virtual void setParent(CCNode* var);
    virtual void setPosition(const CCPoint& position);

};

#endif