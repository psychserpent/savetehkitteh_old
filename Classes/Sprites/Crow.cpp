/*
-------------------------------------------------------------------------------
Crow Sprite (Critter)
-------------------------------------------------------------------------------
*/
#include "Crow.h"

// Constructor
Crow::Crow(CCSprite* target)
{
	m_target = target;

	CCSpriteFrameCache* cache = CCSpriteFrameCache::sharedSpriteFrameCache();
	CCDirector* pDirector = CCDirector::sharedDirector();
	CCSize maxVis = pDirector->getVisibleSize();

	CCAnimation* flyAnim = CCAnimation::create();

	flyAnim->addSpriteFrame(cache->spriteFrameByName("crow0000.png"));
	flyAnim->addSpriteFrame(cache->spriteFrameByName("crow0001.png"));
	flyAnim->addSpriteFrame(cache->spriteFrameByName("crow0002.png"));
	flyAnim->addSpriteFrame(cache->spriteFrameByName("crow0003.png"));
	flyAnim->addSpriteFrame(cache->spriteFrameByName("crow0004.png"));
	flyAnim->setDelayPerUnit(0.05f);
	
	CCAnimation* swoopAnim = CCAnimation::create();

	swoopAnim->addSpriteFrame(cache->spriteFrameByName("crow0000.png"));
	swoopAnim->setDelayPerUnit(1.f);

	m_flap = CCRepeatForever::create(CCAnimate::create(flyAnim));
	m_swoop = CCRepeatForever::create(CCAnimate::create(swoopAnim));

	CC_SAFE_RETAIN(m_flap);
	CC_SAFE_RETAIN(m_swoop);
	
	m_flyRegion = CCRectMake(0, maxVis.height - 300.f, maxVis.width, 300.f);
	
	CCLOG("Fly Region x = %f", m_flyRegion.origin.x);
	CCLOG("Fly Region y = %f", m_flyRegion.origin.y);
	CCLOG("Fly Region width = %f", m_flyRegion.size.width);
	CCLOG("Fly Region height = %f", m_flyRegion.size.height);

}


Crow::~Crow(void)
{
}

Crow* Crow::create(CCSprite* target) {
	Crow* pRet = new Crow(target);
	if(pRet && pRet->init())
	{
		pRet->autorelease();
		return pRet;
	}
	else {
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

// Initializer for the crow
bool Crow::init() 
{
	if(!CCSprite::init())
	{
		return false;
	}
	
	//this->getTexture()->setAliasTexParameters();
	this->setScale(2.f);
	this->setAnchorPoint(ccp(0.5f, 0.5f));
	
	this->currentState = CROW_FLY;
	this->think();
	
	return true;
}

// Get's the crow's hover action
CCAction* Crow::getHoverAction() {
	// This is a pretty basic action, should just involve the crow flapping in
	// place.

	float delay = (CCRANDOM_0_1() * 2.0f) + 2.0f;

	CCDelayTime* delayAction = CCDelayTime::create(delay);
	CCCallFunc* callback = CCCallFunc::create(this, callfunc_selector(Crow::think));

	CCSequence* sequence = CCSequence::createWithTwoActions(delayAction, callback);

	return sequence;

}

// Get's the crow's fly action
CCAction* Crow::getFlyAction() {
	// This should involve flying from one point on the screen to another above
	// a specific height.

	float moveX = (CCRANDOM_0_1() * m_flyRegion.size.width) + m_flyRegion.origin.x;
	float moveY = (CCRANDOM_0_1() * m_flyRegion.size.height) + m_flyRegion.origin.y;

	CCLOG("Fly region: %.2f, %.2f, %.2f, %.2f", m_flyRegion.origin.x, m_flyRegion.origin.y
											  , m_flyRegion.size.width, m_flyRegion.size.height);

	float dir = this->getScaleX();

	if((moveX < this->getPositionX() && dir > 0) || (moveX > this->getPositionX() && dir < 0))
		this->setScaleX(dir * -1);


	CCLOG("Moving crow to position: %.2f, %.2f", moveX, moveY);
	CCActionInterval* movePosition = CCMoveTo::create(2.f, ccp(moveX, moveY));

	CCEaseSineInOut* moveAction = CCEaseSineInOut::create(movePosition);

	CCCallFunc* callback = CCCallFunc::create(this, callfunc_selector(Crow::think));

	CCSequence* sequence = CCSequence::createWithTwoActions(moveAction, callback);

	return sequence;

}

CCAction* Crow::getSwoopAction() {

	//CCPointArray* points = CCPointArray::create(3);

	CCPoint origin = this->getPosition();
	CCPoint target = m_target->getPosition();

	float besYmod = (origin.y - target.y) * -1.2f;

	target.y += besYmod + 60.f;

	float destX = (target.x - origin.x) * 2.f + origin.x; // This is weird but makes sense.
	
	float dir = this->getScaleX();
	if((destX < this->getPositionX() && dir > 0) || (destX > this->getPositionX() && dir < 0))
		this->setScaleX(dir * -1);

	CCPoint destination = ccp(destX, origin.y);
	
	//points->addControlPoint(origin);
	//points->addControlPoint(target);
	//points->addControlPoint(destination);
	
	//CCLOG("--origin .x: %.2f .y: %.2f", origin.x, origin.y);
	//CCLOG("--target .x: %.2f .y: %.2f", target.x, target.y);
	//CCLOG("--destination .x: %.2f .y: %.2f", destination.x, destination.y);

	//CCCatmullRomTo* moveAction = CCCatmullRomTo::create(1.5f, points);

	ccBezierConfig bConfig;
	bConfig.controlPoint_1 = origin;
	bConfig.controlPoint_2 = target;
	bConfig.endPosition = destination;

	CCBezierTo* moveAction = CCBezierTo::create(1.5f, bConfig);

	CCCallFunc* callback = CCCallFunc::create(this, callfunc_selector(Crow::think));

	CCSequence* sequence = CCSequence::createWithTwoActions(moveAction, callback);

	return sequence;
}

// Crow's game logic.
void Crow::update(float dt)
{

}

// Crow's primitive AI.
void Crow::think()
{

	CCLog("Crow thought about something...");

	this->stopAllActions();
	
	int action = CROW_HOVER;
	if(currentState == CROW_HOVER)	
		// If it's already hovering we make it randomly fly or swoop.
		action = rand() % 2 + 1;
	else if (currentState == CROW_FLY)
		// If it's flying, we want it to stop and hover for a bit.
		action = CROW_HOVER;
	else if (currentState == CROW_SWOOP)
		// If it's swooping, we want it to stop and hover for a bit again.
		action = CROW_HOVER;

	if(action == CROW_HOVER) {
		CCLog("Crow is going to hover.");
		this->runAction(m_flap);
		this->runAction(this->getHoverAction());
	}
	else if(action == CROW_FLY) {
		CCLog("Crow is going to fly.");
		this->runAction(m_flap);
		this->runAction(this->getFlyAction());
	}
	else if(action == CROW_SWOOP) {
		CCLog("OMG! CROW IS SWOOPING! RUN KITTEH!!! D:");
		this->runAction(m_swoop);
		this->runAction(this->getSwoopAction());
	}

	currentState = action;

}
