/************************************************************************

	Game UI

************************************************************************/
#include "GameUI.h"



bool GameUI::init() {
	bool result = false;

	if(CCLayer::init())
	{
		statTime = CCLabelTTF::create("", "Helvetica", 14, CCSizeMake(100.f, 30.f), kCCTextAlignmentRight);
		statTime->setColor(ccc3(0, 0, 0));
		statTime->setPosition(ccp(20.f, 20.f));

		this->addChild(statTime);

		result = true;
	}

	return result;
}

void GameUI::displayTime(float time) {
	this->statTime->setString(CCString::createWithFormat("%4.2f", time)->getCString());
}