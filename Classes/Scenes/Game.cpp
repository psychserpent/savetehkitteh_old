/*****************************************************************************\

	Main Scene

*******************************************************************************

	The main scene is the stage for the game.

\*****************************************************************************/
#include "Game.h"

#define Z_FRONT 1
#define Z_BACK  0

// Constructor
Game::Game(void) 
{	// Variable initialization
	//critters = CCArray::create();
	//props = CCArray::create();
	//player = Kitteh::create();
	backgrounds = CCArray::create();
}

// Destructor
Game::~Game(void) 
{
	//CC_SAFE_RELEASE(gameLayer);
 //   CCSpriteFrameCache::purgeSharedSpriteFrameCache();
}

// Loads all the assets this scene requires.
void Game::initAssets() {
	
	CCSpriteFrameCache* sCache = CCSpriteFrameCache::sharedSpriteFrameCache();

	sCache->addSpriteFramesWithFile("sprites/kitteh.plist");
	sCache->addSpriteFramesWithFile("backgrounds/backgrounds.plist");
	sCache->addSpriteFramesWithFile("sprites/props.plist");
	sCache->addSpriteFramesWithFile("sprites/crow.plist");

	sCache->spriteFrameByName("background0001.png")->getTexture()->setAliasTexParameters();
	sCache->spriteFrameByName("prop0001.png")->getTexture()->setAliasTexParameters();
	sCache->spriteFrameByName("kitteh0000.png")->getTexture()->setAliasTexParameters();
	sCache->spriteFrameByName("crow0000.png")->getTexture()->setAliasTexParameters();

}

// Initializes all the backgrounds and places them accordingly.
void Game::initBackgrounds() {
	
	// Background 0 - goes directly behind game layer / non-parallaxy
	//----------------------------------------------------------------
	CCLayer* background0 = CCLayer::create();
	CCSprite* bg0 = CCSprite::createWithSpriteFrameName("background0001.png");
	bg0->setAnchorPoint(ccp(0,0));
	background0->addChild(bg0);
	backgrounds->insertObject(background0, 0);
	this->addChild(background0, 0);

}

//
// Initializer for main scene.
bool Game::init() 
{
	bool result = false;

	// After much analysis, I've determined that this just calls the init()
	// of the parent class and checks to see if it's false.  If it is, this
	// init returns false.  The reason we do this here instead of just doing
	// return CCScene:init() is because the rest of the init() here will be
	// executed, and we don't want that.  We want to break our nice logical
	// flow here.

	//if( !CCScene::init() ) { 
	//	  return false; 
	//}

	// But because the above sucks so much, I'm going to do proper code 
	// structure. You anti-perfectionist easymode bitches can SUCK IT! >:(
	if(CCScene::init() ) {	
		this->setTag(TAG_GAME);
		// Temporary piece of shit.  It just places the close button on the
		// screen.  We're going to remove this later and replace it with
		// something more AWESOME.
		this->basicJumble();

		// Next, we need to load the assets for our scene. (see method for more 
		// details)
		this->initAssets();
		
		// Next we do backgrounds
		this->initBackgrounds();

		gameUI = GameUI::create();
		this->addChild(gameUI, 2);
		
		gameLayer = GameLayer::create(this);
		this->addChild(gameLayer, 1);
		
        this->_time = 0.f;
		
        this->scheduleUpdate();

		result = true; // Return value.
	}

	return result;
	

}

// Game logic update for the entire scene.
void Game::update(float dt) {
	
	if(this->isRunning()) {
		//CCLOG("running");
        this->_time += dt;

		gameUI->displayTime(this->_time);
	//float x = this->gameLayer->getPositionX();
	//this->gameLayer->setPositionX(x - (dt * 10.f));
    }
}

float Game::getTime() {
    return this->_time;
}


void Game::basicJumble() {
	
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    CCMenuItemImage *pCloseItem = CCMenuItemImage::create(
                                        "CloseNormal.png",
                                        "CloseSelected.png",
                                        this,
                                        menu_selector(Game::menuCloseCallback));
    pCloseItem->setAnchorPoint(ccp(1.0f, 1.0f));
	pCloseItem->setPosition(ccp(visibleSize.width, visibleSize.height));
	pCloseItem->setScale(2.0f);

    // create menu, it's an autorelease object
    CCMenu* pMenu = CCMenu::create(pCloseItem, NULL);
    pMenu->setPosition(CCPointZero);
    this->addChild(pMenu, 1);

}


void Game::menuCloseCallback(CCObject* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) || (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
	CCMessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
#else
    //CCSpriteFrameCache::sharedSpriteFrameCache()->purgeSharedSpriteFrameCache();
    CCDirector::sharedDirector()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
#endif
}

/*****************************************************************************\

	Game Layer

*******************************************************************************

	The main layer with all our main actors.

\*****************************************************************************/

// Constructor
GameLayer::GameLayer(Game* scene) {
	// Member variables
    this->game = scene;
	critters = CCArray::create();
	props = CCArray::create();
    hazards = CCArray::create();
	kitteh = Kitteh::create();

    CC_SAFE_RETAIN(critters);
    CC_SAFE_RETAIN(props);
    CC_SAFE_RETAIN(hazards);
    CC_SAFE_RETAIN(kitteh);

	this->floor = 20.f;
}

// Destructor
GameLayer::~GameLayer(void) {
	CC_SAFE_RELEASE(kitteh);
	CC_SAFE_RELEASE(props);
	CC_SAFE_RELEASE(critters);
    CC_SAFE_RELEASE(hazards);
}

GameLayer* GameLayer::create(Game* scene) {
    GameLayer* pRet = new GameLayer(scene);
    if (pRet && pRet->init()) {
        pRet->autorelease();
        return pRet;
    }
    else { delete pRet; pRet = NULL; return NULL; } // Cleans shit up.
}

// Initializes the layer and makes it ready for play.
bool GameLayer::init() {
	bool result = false; // Return value.

	if(CCLayer::init()) { // Check if these exist.
		
		this->setTag(TAG_GAMELAYER);

		// Set gravity
		gravity = ccp(0.f, -900.f);

		kitteh->setFoot(ccp(600, floor));
		this->addChild(kitteh, Z_FRONT);

		LampStand* ls = LampStand::createWithGravity(gravity);
        ls->setHazardArray(this->hazards);
		ls->setPosition(ccp(510, floor));
		this->addChild(ls, Z_BACK);

        props->addObject(ls);

        //LampStand* lampStand = LampStand::create(gravity);
		//this->props->addObject(lampStand);
        //lampStand->setPosition(ccp(300, floor));
		//temp = lampStand;
        //this->addChild(lampStand, Z_BACK);

		this->scheduleUpdate();

		result = true;
	}

	return result;
}

// The main game loop.
void GameLayer::update(float dt) {
	
    CCObject* obj;
    CCARRAY_FOREACH_REVERSE(hazards, obj)
    {
        PropHazard* hazard = dynamic_cast<PropHazard*>(obj);
        if(hazard != NULL && !hazard->isAttached())
        {
            CCRect box = hazard->boundingBox();
            if(floor > box.getMinY()){
                hazards->removeObject(hazard);
                hazard->destroy();
            }

			if(hazard->isGrabbed()) {
				this->tryAttach(hazard);
			}
        }
    }

    CCARRAY_FOREACH(props, obj) {
        PropBase* prop = dynamic_cast<PropBase*>(obj);
        // Check to see if on the same floor.
        CCPoint p = prop->getPosition();
        CCPoint k = kitteh->getFoot();
        if(p.y < k.y + 2.f && p.y > k.y - 2.f) // This should be a reasonable range leeway.
        {
            // now to qualify range
            float distance = fabsf(p.x - k.x);
            if(distance <= KITTEH_MAX_TARGET_RANGE) {
                kitteh->setTarget(prop);
            }
        }
    }
    
}




void GameLayer::tryAttach(PropHazard* hazard) {
	CCObject* obj;
	bool found = false;
	for(int i = 0; i < this->getChildrenCount() && !found; i++) {
		obj = this->getChildren()->objectAtIndex(i);
		PropBase* prop = dynamic_cast<PropBase*>(obj);
		if(prop != NULL) found = prop->tryAttach(hazard);
	}
}