/*
-------------------------------------------------------------------------------
Crow Sprite (Critter)
-------------------------------------------------------------------------------
A crow is a vicious little critter that flies down and attacks the helpless
kitteh.  Tapping the crow will kill it, but it can also be grabbed and flinged
away instead. :3
*/
#include "Critter.h"



#define CROW_HOVER 0	        // Crow is just flapping in place - not moving.
#define CROW_FLY 1			// Crow is flying in a direction - not attacking.
#define CROW_SWOOP 2			// Crow is swooping down to hit the poor kitteh.


class Crow : public Critter
{
private:
	CCAction* m_flap;
	CCAction* m_swoop;

	CCRect m_flyRegion;

	void think();			// AI logic for the crow.

	int currentState;		// The crow's current status.
	
	CCAction* getHoverAction();
	CCAction* getSwoopAction();
	CCAction* getFlyAction();

	CCSprite* m_target;

public:
	Crow(CCSprite* target);
	~Crow(void);

	//CREATE_FUNC(Crow);
	static Crow* create(CCSprite* target);

	virtual bool init();

	virtual void update(float dt);


};

