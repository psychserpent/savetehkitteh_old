#ifndef __PROPHAZARD_H__
#define __PROPHAZARD_H__
/*
Prop Hazard
------------------------------------------------------------------
Prop hazards affix themselves to prop bases.  When a prop is
"bumped", it's hazard props are also bumped.
*/
#include "cocos2d.h"
#include "PropBase.h"
#include "Debris.h"
#include "Macros.h"
#include "Tags.h"

USING_NS_CC;	// Cocos2d Namespace

class PropAttachPoint;

class PropHazard : public CCSprite, public CCTargetedTouchDelegate
{
private:
	// Member variables
	CCPoint _gravity;
	CCPoint _velocity;
    float _tilt;    // Tilting mechanism... stfu it makes sense. :P
    PropAttachPoint* _attachedTo;
    int _baseTag;

	bool _grabbed;
    // Methods
protected:
	//CCNode* detatch();
	//void attach(CCNode* node);
	void move(float dt);
public:
	// Constructor
	PropHazard(void);
	~PropHazard(void);

	CREATE_FUNC(PropHazard);
    CREATE_FUNC_WITH_GRAVITY(PropHazard);

	// Initialization
	virtual bool init();
	virtual bool initWithGravity(const CCPoint& _gravity);
    
	// Properties
    //-------------------------------------------------------------------------
    int getBaseTag();
    void setBaseTag(int nTag);

	bool isGrabbed();

	const CCPoint& getGravity();
	void setGravity(const CCPoint& gravity);

    const CCPoint& getVelocity();
    void setVelocity(const CCPoint& velocity);

    void setTilt(float tilt);
    
	// Methods
    //-------------------------------------------------------------------------
    bool isAttached();
    void attach(PropAttachPoint* attachPoint);
    void detach();
	
	// Overriden Methods
    //-------------------------------------------------------------------------
	virtual void destroy();		// Destroys the object.
	virtual void update(float dt);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);

};

#endif