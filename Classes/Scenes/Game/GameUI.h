#ifndef __GAMEUI_H__
#define __GAMEUI_H__
/*
User Interface layer for the Game scene
------------------------------------------------------------------
*/
#include "cocos2d.h"

USING_NS_CC;

class GameUI : public CCLayer
{
private:
	CCLabelTTF* statTime;
public:
	GameUI() { };
	~GameUI() { };

	CREATE_FUNC(GameUI);
	virtual bool init();

	void displayTime(float time);
};




#endif