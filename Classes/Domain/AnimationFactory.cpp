#include "AnimationFactory.h"
/******************************************************************************

	Animation Factory

*******************************************************************************

Okay, so from reading online and shit, yah, I know, animations should really be
managed in a file -- don't care.  However, because I might change the way this
sort of stuff is handled in the future, I'm creating this animation factory to
serve as a handler for all animations.

*/

// The below method will fetch animations as needed.
CCAnimation* AnimationFactory::getAnim(eAnim anim) {

	CCAnimation* result = CCAnimation::create();
	CCSpriteFrameCache* cache = CCSpriteFrameCache::sharedSpriteFrameCache();

	// The master switch containing all animations.  Change the below animation
	// configuration here.
	switch(anim) {
	case animKittehSit: // Kitteh Sit Animation
		//---------------------------------------------------------------------

		result->addSpriteFrame(cache->spriteFrameByName("kitteh0000.png"));
		result->setDelayPerUnit(1.f);

		break;
	case animKittehWalk: // Kitteh Walk Animation
		//---------------------------------------------------------------------

		result->addSpriteFrame(cache->spriteFrameByName("kitteh0001.png"));
		result->addSpriteFrame(cache->spriteFrameByName("kitteh0002.png"));
		result->addSpriteFrame(cache->spriteFrameByName("kitteh0003.png"));
		result->addSpriteFrame(cache->spriteFrameByName("kitteh0004.png"));
		result->setDelayPerUnit(0.2f);

		break;
	case animKittehHurt: // Kitteh Hurt Animation
		//---------------------------------------------------------------------

		result->addSpriteFrame(cache->spriteFrameByName("kitteh0005.png"));
		result->setDelayPerUnit(1.f);

		break;
	case animKittehLook: // Kitteh Look Animation
		//---------------------------------------------------------------------
		
		result->addSpriteFrame(cache->spriteFrameByName("kitteh0006.png"));
		result->setDelayPerUnit(1.f);

		break;
	case animKittehAttack: // Kitteh Attack Animation
		//---------------------------------------------------------------------

		result->addSpriteFrame(cache->spriteFrameByName("kitteh0007.png"));
		result->addSpriteFrame(cache->spriteFrameByName("kitteh0006.png"));
        result->setDelayPerUnit(0.1f);

		break;
	case animCrowFlap: // Crow Flap Animation
		//---------------------------------------------------------------------

		// TODO

		break;
	case animCrowSwoop:
		//---------------------------------------------------------------------

		// TODO

		break;
	case animCrowHurt:

		// TODO

		break;
	}

	return result;
}