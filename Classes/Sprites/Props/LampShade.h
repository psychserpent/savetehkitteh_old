#ifndef __LAMPSHADE_H__
#define __LAMPSHADE_H__
/*
Prop Hazard
------------------------------------------------------------------
Prop hazards affix themselves to prop bases.  When a prop is
"bumped", it's hazard props are also bumped.
*/
#include "cocos2d.h"
#include "Macros.h"
#include "PropHazard.h"
#include "Debris.h"
#include "Tags.h"

USING_NS_CC;		// Cocos2d Namespace

class LampShade : public PropHazard
{
private:
public:
	// Constructors
    //-------------------------------------------------------------------------
	LampShade(void);
	~LampShade(void);

	CREATE_FUNC(LampShade);
	CREATE_FUNC_WITH_GRAVITY(LampShade);
    
	// Methods
    //-------------------------------------------------------------------------
	virtual bool init();
	virtual bool initWithGravity(const CCPoint& gravity);

};

#endif