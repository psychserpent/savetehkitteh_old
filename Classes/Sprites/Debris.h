#ifndef __DEBRIS_H__
#define __DEBRIS_H__
/*
Debris
------------------------------------------------------------------
Debris is just junk that appears then disappears or w/e.

The life-span of this thing should be pretty short, like not
even more than a few seconds.
*/


#include "cocos2d.h"
#include "Macros.h"
#include "PropHazard.h"

USING_NS_CC;		// Cocos2d Namespace

#define MAX_DEBRIS_LIFESPAN 0.4f

class Debris : public CCSprite
{
private:
	CCPoint _gravity;
	CCPoint _velocity;

	float life;
	float _tilt;
public:
	Debris(const CCPoint& gravity) { life = 0; _gravity = gravity; };
	~Debris(void) { };

	CREATE_DEBRIS(Debris);
	virtual bool initWithSpriteFrame(CCSpriteFrame* spriteFrame);


	void setVelocity(const CCPoint& velocity);
	void setTilt(float tilt);

	virtual void update(float dt);
};


#endif