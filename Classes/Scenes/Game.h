#ifndef __GAME_H__
#define __GAME_H__
/*
Game Scene
------------------------------------------------------------------
This is the main game scene where all the action happens.  It
contains a collection of layers.
*/

#include "cocos2d.h"
#include "Kitteh.h"
#include "LampStand.h"
#include "Crow.h"
#include "GameUI.h"
#include "Tags.h"
#include <cmath>

#define PTM_RATIO 128

USING_NS_CC;


// Main game scene
class Game : public CCScene
{
private:

	// Variables
	//-----------------------
	CCLayer* gameLayer;		// Main game layer, this is where the action happens.
	GameUI* gameUI;			// UI Layer
	CCArray* backgrounds;	// Background layers (could be several).
    float _time;            // The elapsed time of the scene.

	// Methods
	//-----------------------
	void initAssets();		// Responsible for loading all the sounds/assets.
	void initBackgrounds();	// Responsible for preparing the other layers.

	//-------
	void basicJumble(); // Temporary garbage

public:
	Game(void);
	~Game(void);

	CREATE_FUNC(Game);		// create() method
	virtual bool init();	// Initialization
	virtual void update(float dt);	// Update logic for the scene.

    float getTime();        // Gets that sweet sweet elapsed time.

	//-------
    void menuCloseCallback(CCObject* pSender);	// Temporary garbage
};


// Main game layer
class GameLayer : public CCLayer
{
private:
	
	Kitteh* kitteh;		// The star of the show.  IT'S A KITTEH!!! =^-^=

	CCArray* critters;	// An array of enemy critters
	CCArray* props;		// An array of props
    CCArray* hazards;   // An array of prop hazards

	CCPoint gravity;	// Accelleration downward by gravity.

	float floor;		// The floor of the layer (fixed number for now).

    Game* game;
public:
	GameLayer(Game* scene);
	~GameLayer(void);
	static GameLayer* create(Game* scene); // Create method - Not using
                                           // CREATE_FUNC() here cuz it's ghey.

	void tryAttach(PropHazard* hazard); // Attempts to attach a PropHazard to something.

	virtual bool init();	// Initializer for the game layer.
	
	//virtual void draw();
	virtual void update(float dt);	// Update logic for the game layer.

};

#endif // __GAME_H__