#include "PropBase.h"
/************************************************************************

	PropBase Sprite

*************************************************************************/

// Constructor / Destructor
//-----------------------------------------------------------------------------
PropBase::PropBase()  { _gravity = ccp(0,0); this->_attachPoints = new CCArray(); this->_attachPoints->initWithCapacity(1); }
PropBase::~PropBase() { CC_SAFE_DELETE(_attachPoints); }

// Properties
//-----------------------------------------------------------------------------
const CCPoint&  PropBase::getGravity()                          { return _gravity; }
void            PropBase::setGravity(const CCPoint& gravity)    { _gravity = gravity; }

CCArray*        PropBase::getAttachPoints()                     { return this->_attachPoints; }
void            PropBase::addAttachPoint(PropAttachPoint* attachPoint) 
                    { _attachPoints->addObject(attachPoint); }

CCArray*        PropBase::getHazardArray() { return _hazardArray; }
void            PropBase::setHazardArray(CCArray* arr) {
    assert(arr != NULL);

    _hazardArray = arr;
    CCObject* item = NULL;
    CCARRAY_FOREACH(this->getAttachPoints(), item) {
        PropAttachPoint* atPoint = dynamic_cast<PropAttachPoint*>(item);
        // Test to see if...
        if(    atPoint != NULL // Safety Check
            && atPoint->getAttached() != NULL // Has an attachment
            && !_hazardArray->containsObject(atPoint->getAttached()) // Array doesn't already have
            )
                _hazardArray->addObject(atPoint->getAttached());
            
    }
}

bool            PropBase::hasAttached()
{
    bool result = false;
    CCArray* atPoints = this->getAttachPoints();
    if(atPoints && atPoints->data->num > 0) {
        for(int i = atPoints->count() - 1; i >= 0; i--)
        {
            PropAttachPoint* at = dynamic_cast<PropAttachPoint*>(atPoints->objectAtIndex(i));
            if(at->hasAttached()) { result = true; i = -1; }
        }
    }
    return result;
}
// Methods
//-----------------------------------------------------------------------------
bool PropBase::init() {
	return CCSprite::init();
}

bool PropBase::initWithGravity(const CCPoint& gravity) {
    if(!CCSprite::init()) return false;

    _gravity = gravity;

    return true;
}

// Bump Action - Causes hazards to dislodge.
void PropBase::bump(CCNode* collider) {

    CCMoveBy* left1 = CCMoveBy::create(0.03f, ccp(-3.f, 0));
    CCMoveBy* right2 = CCMoveBy::create(0.06f, ccp(6.f, 0));
    CCMoveBy* left3 = CCMoveBy::create(0.03f, ccp(-3.f, 0));

    CCCallFuncND* callback = CCCallFuncND::create(this, callfuncND_selector(PropBase::loose), (void*)collider);
    
    CCArray* seq = CCArray::createWithCapacity(4);
    seq->addObject(left1);
    seq->addObject(right2);
    seq->addObject(left3);
    seq->addObject(callback);

    CCSequence* action = CCSequence::create(seq);

    this->runAction(action);

}

// Looses the hazards
void PropBase::loose(CCNode* sender, void* data) {
    CCNode* collider = (CCNode*)data;
	CCArray* atPoints = this->getAttachPoints();
	CCObject* item = NULL;
	//CCLOG("bumping...");
	CCARRAY_FOREACH(atPoints, item) {
		PropAttachPoint* attachPoint = dynamic_cast<PropAttachPoint*>(item);
		if(attachPoint && attachPoint->hasAttached()) {
            PropHazard* hazard = attachPoint->getAttached();
            CCPoint d = getDirection(collider->getPosition());
            hazard->setVelocity(ccp(d.x * 40.f, 200.f));
            hazard->setRotation(d.x * 10.f);
            hazard->setTilt(d.x * 10.f);
            hazard->detach();
        }
	}

}

const CCPoint& PropBase::getDirection(const CCPoint& target) {
    CCPoint position = this->getPosition();
    if(position.x < target.x) return ccp(1.f, 0);
    else return ccp(-1.f, 0);
}

void PropBase::setParent(CCNode* var)
{
    CCSprite::setParent(var);

    // Loop through and add all attach point hazards to the parent as well.
	CCArray* atPoints = this->getAttachPoints();
	CCObject* item;
    CCARRAY_FOREACH(atPoints, item) {
        PropAttachPoint* attachPoint = dynamic_cast<PropAttachPoint*>(item);
        if(attachPoint->hasAttached())
            var->addChild(attachPoint->getAttached(), this->getZOrder()+1);
    }
}

void PropBase::setPosition(const CCPoint& position)
{
    CCSprite::setPosition(position);

    // Loop through and add all attach point hazards to the parent as well.
	CCArray* atPoints = this->getAttachPoints();
	CCObject* item;
    CCARRAY_FOREACH(atPoints, item) {
        PropAttachPoint* attachPoint = dynamic_cast<PropAttachPoint*>(item);
        attachPoint->getAttached()->setPosition(position + attachPoint->getPosition());
    }
}


bool PropBase::tryAttach(PropHazard* hazard) {
	CCObject* item;
	bool result = false;
	CCArray* atPoints = this->getAttachPoints();
	CCARRAY_FOREACH(atPoints, item) {
		PropAttachPoint* atPoint = dynamic_cast<PropAttachPoint*>(item);
		float distance = hazard->getPosition().getDistance(atPoint->getPosition() + this->getPosition());
		if(!atPoint->hasAttached() && (distance < 15.f)) {
			hazard->attach(atPoint);
			hazard->setPosition(atPoint->getPosition() + this->getPosition());
			result = true;
		}
	}
	return result;
}