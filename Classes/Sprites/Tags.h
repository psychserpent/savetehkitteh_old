#ifndef __TAGS_H__
#define __TAGS_H__
/*
Tag list
------------------------------------------------------------------
*/

#define TAG_GAME		  0 // Tag for the main scene.
#define TAG_GAMELAYER	  1 // Tag for the game layer.
#define TAG_UILAYER		  2 // Tag for the UI layer.

#define TAG_KITTEH		  5 // Tag for the kitteh.
#define TAG_CRITTER		  6 // Tag for a vile critter.

#define TAG_PROPBASE	 11 // Tag for the prop base.
#define TAG_PROPHAZARD	 12 // Tag for the prop hazard.


// PropHazards

#define TAG_LAMPSHADE	101	// Tag for the lamp stand prop base.

#endif