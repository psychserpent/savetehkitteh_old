#include "Kitteh.h"
/*****************************************************************************\

	Kitteh Sprite

\*****************************************************************************/

// ... Yah I gotta rewrite most of this shit.

// Constructor / Destructor
//-----------------------------------------------------------------------------
Kitteh::Kitteh() {
    // Initial Variables
	//this->_facing = ccp(1.f, 0); // Kitteh faces the right when starting.
    this->_time = 0;
    this->_state = 0;


    // Animations
	CCAnimation* animWalk   = AnimationFactory::getAnim(animKittehWalk);
	CCAnimation* animSit    = AnimationFactory::getAnim(animKittehSit);
	CCAnimation* animHurt   = AnimationFactory::getAnim(animKittehHurt);
    CCAnimation* animLook   = AnimationFactory::getAnim(animKittehLook);
    CCAnimation* animAttack = AnimationFactory::getAnim(animKittehAttack);

	this->_walk     = CCRepeatForever::create(CCAnimate::create(animWalk));
	this->_sit      = CCRepeatForever::create(CCAnimate::create(animSit));
	this->_hurt     = CCRepeatForever::create(CCAnimate::create(animHurt));
    this->_look     = CCRepeatForever::create(CCAnimate::create(animLook));
    this->_attack   = CCAnimate::create(animAttack);

	CC_SAFE_RETAIN(this->_walk);
	CC_SAFE_RETAIN(this->_sit);
	CC_SAFE_RETAIN(this->_hurt);
	CC_SAFE_RETAIN(this->_look);
    CC_SAFE_RETAIN(this->_attack);
}

Kitteh::~Kitteh(void) { 
	CC_SAFE_RELEASE(this->_walk);
	CC_SAFE_RELEASE(this->_sit);
	CC_SAFE_RELEASE(this->_hurt);
	CC_SAFE_RELEASE(this->_look);
    CC_SAFE_RELEASE(this->_attack);
}

// Properties
//-----------------------------------------------------------------------------
float           Kitteh::getFacing() 
{ 
    if(!this->getScaleX()) return 0;
    return this->getScaleX()/std::fabsf(this->getScaleX()) * KITTEH_FACING_CORRECTION;
}
void            Kitteh::faceLeft() 
{ this->setScaleX(std::fabsf(this->getScaleX()) * -1.f * KITTEH_FACING_CORRECTION); }
void            Kitteh::faceRight()
{ this->setScaleX(std::fabsf(this->getScaleX()) * KITTEH_FACING_CORRECTION); }

void            Kitteh::setVelocity(const CCPoint& velocity) { _velocity = velocity; }
const CCPoint&  Kitteh::getVelocity()                    { return _velocity; }

void            Kitteh::setState(int state)              { _state = state; }
int             Kitteh::getState()                       { return _state; }

void            Kitteh::stepTime(float dt)               { _time += dt; }
float           Kitteh::getTime()                        { return _time; }

const CCPoint&  Kitteh::getFoot() // Gets the bottom of the kitteh sprite.
{ return ccp(this->boundingBox().getMidX(), this->boundingBox().getMinY()); }
void            Kitteh::setFoot(const CCPoint& foot) 
{ this->setPosition(ccp(foot.x, foot.y + this->boundingBox().size.height * 0.5f)); }

void            Kitteh::setNextThought(float dur) { _nextThought = _time + dur; }
void            Kitteh::setInvincibility(float dur) { _invincibility = _time + dur; }

bool            Kitteh::isInvincible() { return (_invincibility > _time); }

void            Kitteh::setTarget(PropBase* target) 
{ 
    //CCLOG("Kitteh received a target...");
    if(target != _oldTarget && target != _target) { 
        CCLOG("Kitteh now has a target...");
        // TODO: Random chance of target getting picked.
        expireTarget();
        _target = target; 
        //faceTarget();
        walkTarget();
        setState(KITTEH_WALK_TARGET);
    } 
}
PropBase*       Kitteh::getTarget() { return _target; }
bool            Kitteh::hasTarget() { return _target; }

// Methods
//-----------------------------------------------------------------------------

// Initialization of teh kitteh. :3
bool Kitteh::init() {
	if(!CCSprite::initWithSpriteFrameName("kitteh0000.png")) return false;
	
	this->setState(KITTEH_SIT);

	this->setScale(2.f);
	this->setAnchorPoint(ccp(0.5f, 0.5f));

	this->think();

	this->scheduleUpdateWithPriority(-1);

	return true;
}

void Kitteh::update(float dt) {
    
    // Check the kitteh state
    switch(_state) {
    case KITTEH_WALK_TARGET:
        if(!_target) // If target disappeared for any reason, do sum thinkin.
            this->think();
        else if(getFacing() < 0 && _target->getPositionX() + 20.f >= this->getPositionX()) {
            this->setPositionX(_target->getPositionX() + 20.f);
            this->lookAt(_target);
            //expireTarget();
        }
        else if(getFacing() > 0 && _target->getPositionX() - 20.f <= this->getPositionX()) {
            this->setPositionX(_target->getPositionX() - 20.f);
            this->lookAt(_target);
            //expireTarget();
        }
        break;
    case KITTEH_HURT:

        break;
    case KITTEH_LOOKPROP:
        if(_nextThought <= _time) // After the kitteh has looked long enough.
        {
            float roll = CCRANDOM_0_1();
            if(roll <= KITTEH_ATTACK_CHANCE)
                this->attack(_target);
            else
            {
                expireTarget();
                this->think();
            }
        }
        break;
    case KITTEH_ATTACKPROP:

        break;
    default:
        if(_nextThought <= _time) // If we're past the point of next thought.
            this->think();
        break;
    }

    // Check if old target expired
    if(_targetExpire <= _time) _oldTarget = NULL; 

    this->move(dt);
    this->stepTime(dt);
}

void Kitteh::move(float dt) {
    this->setPosition(this->getPosition() + _velocity * dt);
}

void Kitteh::think() {
    CCLOG("Kitteh thought about something...");

    int action = rand() % 3;

    if(action != _state)
    { // We do this check because if it's the same, keep the animation going.
        this->stopAllActions();

        switch(action) {
        case KITTEH_SIT:
            this->setVelocity(ccp(0,0));
            this->runAction(_sit);
            this->setNextThought(CCRANDOM_0_1() * KITTEH_MAX_SIT_TIME + 1.f);
            break;
        case KITTEH_WALK_LEFT:
            this->faceLeft();
            this->setVelocity(ccp(KITTEH_SPEED * -1.f, 0));
            this->runAction(_walk);
            this->setNextThought(CCRANDOM_0_1() * KITTEH_MAX_WALK_TIME + 1.f);
            break;
        case KITTEH_WALK_RIGHT:
            this->faceRight();
            this->setVelocity(ccp(KITTEH_SPEED, 0));
            this->runAction(_walk);
            this->setNextThought(CCRANDOM_0_1() * KITTEH_MAX_WALK_TIME + 1.f);
            break;
        }
    }

    this->setState(action);
    
}

void Kitteh::collide(CCSprite* collider) {
    if(!isInvincible()) {
        this->hurt();
    }
}

void Kitteh::hurt() {
	this->stopAllActions();

	this->setState(KITTEH_HURT);
	this->setInvincibility(KITTEH_INVINCIBILITY);

	float destination = this->getPositionX() + (40.f * this->getFacing() * -1.f);

	CCJumpTo* jump = CCJumpTo::create(KITTEH_HURT_JUMP, 
        ccp(destination, this->getPositionY()), 20.f, 1);
	CCCallFunc* callback = CCCallFunc::create(this, callfunc_selector(Kitteh::recover));

    unsigned int blinks = (unsigned int)(KITTEH_INVINCIBILITY * 10.f);
	CCBlink* blink = CCBlink::create(KITTEH_INVINCIBILITY, blinks);
	
	CCSequence* moveAction = CCSequence::createWithTwoActions(jump, callback);

	this->runAction(moveAction);
	this->runAction(_hurt);
	this->runAction(blink);

}

// This is the kitteh recover logic that occurs at the end fo the hurt logic.
void Kitteh::recover() {
    this->think();
}

// Kitteh looks at a prop
void Kitteh::lookAt(PropBase* prop) {
    assert(prop != NULL);

    CCLOG("Kitteh is looking at something.");

    this->stopAllActions();
    this->setVelocity(ccp(0,0));

    this->setState(KITTEH_LOOKPROP);
    this->setNextThought(CCRANDOM_0_1() * KITTEH_MAX_LOOK_DELAY + 0.5f);
    
    this->runAction(this->_look);
}

void Kitteh::faceTarget() {
    if(_target) {
        float diff = _target->getPositionX() - this->getPositionX();
        if(diff != 0) diff = diff/fabsf(diff);
        this->setScaleX(std::fabsf(this->getScaleX()) * diff * KITTEH_FACING_CORRECTION);
    }
}

void Kitteh::walkTarget() {
    if(_target) {
        this->stopAllActions();
        float diff = _target->getPositionX() - this->getPositionX();
        if(diff != 0) diff = diff/fabsf(diff);
        this->setScaleX(std::fabsf(this->getScaleX()) * diff * KITTEH_FACING_CORRECTION);
        this->setVelocity(ccp(diff * KITTEH_SPEED, 0));
        this->runAction(_walk);
    }
}

void Kitteh::expireTarget() {
    if(_target != NULL) {
        _oldTarget = _target;
        _target = NULL;
        _targetExpire = _time + KITTEH_TARGET_EXPIRE;
        if(_state == KITTEH_WALK_TARGET) // Just in case.
            this->think();
    }
}

void Kitteh::attack(PropBase* prop) 
{
    assert(prop != NULL);

    this->stopAllActions();
    this->setState(KITTEH_ATTACKPROP);

    // Build sequence of attack anim, then function callback.
    CCCallFunc* followUp = CCCallFunc::create(this, callfunc_selector(Kitteh::attackComplete));
    CCSequence* action = CCSequence::createWithTwoActions(this->_attack, followUp);
    
    _target->bump(this);
    this->runAction(action);
}

void Kitteh::attackComplete() {
    expireTarget();
    this->think();
}