#include "LampStand.h"
/************************************************************************

	Lamp Sprite

*************************************************************************/

// Constructor / Destructor
//-----------------------------------------------------------------------------
LampStand::LampStand()  { this->setTag(TAG_PROPBASE); }
LampStand::~LampStand() { }

// Methods
//-----------------------------------------------------------------------------
void LampStand::_init() {
    
    this->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("prop0002.png"));
    this->setAnchorPoint(ccp(0.5f, 0));
    this->setScale(2.f);

    PropAttachPoint* attach1 = PropAttachPoint::create(ccp(0.f,350));
    //attach1->retain();
    this->addAttachPoint(attach1);

    LampShade* shade = LampShade::createWithGravity(this->getGravity());
    //shade->retain();

    attach1->setAttached(shade);
}

bool LampStand::init() {
    if(!PropBase::init()) return false;

    this->_init();

    return true;
}

bool LampStand::initWithGravity(const CCPoint& gravity) {
    if(!PropBase::initWithGravity(gravity)) return false;
    
    this->_init();

    return true;
}