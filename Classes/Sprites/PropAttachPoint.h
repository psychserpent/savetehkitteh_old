#ifndef __PROPATTACHPOINT_H__
#define __PROPATTACHPOINT_H__
/*
PropAttachPoint
------------------------------------------------------------------
Attach points are where prop hazards can be re-attached to or detached from.

Notes:
	Revising this as of February something of something day...
*/
#include "cocos2d.h"

USING_NS_CC;	// Cocos2d Namespace

class PropHazard; // Cross-Reference

class PropAttachPoint : public CCObject
{
private:
    CCPoint _position;
    PropHazard* _attached;
public:
    // Constructor / Destructor
    //-------------------------------------------------------------------------
    PropAttachPoint(void);
    ~PropAttachPoint(void);
    
    static PropAttachPoint* create(const CCPoint& position);

    // Properties
    //-------------------------------------------------------------------------
    void setPosition(const CCPoint& position);
    const CCPoint& getPosition();

    PropHazard* getAttached();
    void setAttached(PropHazard* attached);
    void detach();

    bool hasAttached();
    
    // Methods
    //-------------------------------------------------------------------------
    bool init(const CCPoint& position);

};

#endif