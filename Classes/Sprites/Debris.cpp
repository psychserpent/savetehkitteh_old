/************************************************************************

	Lamp Shade

*************************************************************************/
#include "Debris.h"

// Initialization
bool Debris::initWithSpriteFrame(CCSpriteFrame* spriteFrame) {
	bool result = false;
	if(CCSprite::initWithSpriteFrame(spriteFrame)) {

		this->_velocity = ccp(0, 0);
		this->_tilt = 0;

		CCFadeOut* fadeOut = CCFadeOut::create(MAX_DEBRIS_LIFESPAN);
		this->runAction(fadeOut);
		//CCBlink* blinkAnim = CCBlink::create(MAX_DEBRIS_LIFESPAN, MAX_DEBRIS_LIFESPAN * 0.5);
		//this->runAction(blinkAnim);

		this->scheduleUpdate();
		result = true;
	}
	return result;
}

// Updates shit...
void Debris::update(float dt) {

	life += dt;

	if(life <= MAX_DEBRIS_LIFESPAN) {
		this->setVelocity(_velocity + _gravity * dt);
		this->setPosition(this->getPosition() + _velocity * dt);
		this->setRotation(this->getRotation() + _tilt * dt);
	}
	else
		this->removeFromParentAndCleanup(true);

}

void Debris::setVelocity(const CCPoint& velocity) {
	this->_velocity = velocity;
}

void Debris::setTilt(float tilt) {
	this->_tilt = tilt;
}