#ifndef __STK_MACROS_H__
#define __STK_MACROS_H__

#define CREATE_PROP(__TYPE__) \
static __TYPE__* create(const CCPoint& gravity) \
{ \
    __TYPE__ *pRet = new __TYPE__(gravity); \
    if (pRet && pRet->init()) \
    { \
        pRet->autorelease(); \
        return pRet; \
    } \
    else \
    { \
        delete pRet; \
        pRet = NULL; \
        return NULL; \
    } \
}

#define CREATE_FUNC_WITH_GRAVITY(__TYPE__) \
static __TYPE__* createWithGravity(const CCPoint& gravity) \
{ \
    __TYPE__ *pRet = new __TYPE__(); \
    if (pRet && pRet->initWithGravity(gravity)) \
    { \
        pRet->autorelease(); \
        return pRet; \
    } \
    else \
    { \
        delete pRet; \
        pRet = NULL; \
        return NULL; \
    } \
}

#define CREATE_DEBRIS(__TYPE__) \
static __TYPE__* create(const CCPoint& gravity, CCSpriteFrame* spriteFrame) \
{ \
    __TYPE__ *pRet = new __TYPE__(gravity); \
    if (pRet && pRet->initWithSpriteFrame(spriteFrame)) \
    { \
        pRet->autorelease(); \
        return pRet; \
    } \
    else \
    { \
        delete pRet; \
        pRet = NULL; \
        return NULL; \
    } \
}

#endif