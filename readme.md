# Game: Save Teh Kitteh

This project is a touch-based video game designed for mobile platforms (Android, iPhone, etc.,).

All code and assets that are NOT under the cocos2d-x game engine or other derived sources are copyrighted (c) 2014 by Jonathan Hand.  The content that was created exclusively for this project are not for public use.

Any placeholder assets that were created by 3rd-party artists/developers are copyrighted by their respective authors.  These assets are for temporary use only and *will not* be part of the finished product.

## What is Save Teh Kitteh?

The Petersons have left their beloved house cat FluffyWinkles alone at home.  Unfortunately, the kitteh has a very bad habit of causing trouble, and without supervision, it would surely hurt itself. :(

It's up to you to make sure this kitteh is safe.

# Files

All my work is under the /Classes and /Resources paths (except for the HelloWorld.png file, that's just junk).

# Game Engine: Cocos2d-x Ver.2.2.1

Cocos2d-x is an open-source mobile 2D game framework, released under MIT License. It is a C*+ version of cocos2d-iphone project. Our focus for cocos2d-x development is around making cocos2d cross platforms. On top of the framework provided by cocos2d-x, mobile games can be written in C*+, Lua or JavaScript, using API that is COMPLETELY COMPATIBLE with that of cocos2d-iphone. Cocos2d-x project can easily be built and run on iOS, Android, Samsung Bada, BlackBerry Qnx, Marmalde etc. Cocos2d-x also supports Windows and Linux, therefore we can debug source code easily and write editors on desktop operating systems. �X� means CROSS, The goal of this open-source project is to allow its users to create cross-platform code.

Website: http://www.cocos2d-x.org