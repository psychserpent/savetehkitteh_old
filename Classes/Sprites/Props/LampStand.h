#ifndef __LAMPSTAND_H__
#define __LAMPSTAND_H__
/*
Lamp Sprite
------------------------------------------------------------------
Lamps consist of lamp stands and lamp heads.  The lamp head will
fall when the lamp stand gets wibbled.
*/
#include "cocos2d.h"
#include "PropBase.h"
#include "Macros.h"
#include "LampShade.h"
#include "Tags.h"

USING_NS_CC;	// Cocos2d Namespace

class LampStand : public PropBase
{
private:
    void _init();
public:
	// Constructors
    //-------------------------------------------------------------------------
	LampStand();
	~LampStand();

	CREATE_FUNC(LampStand);
    CREATE_FUNC_WITH_GRAVITY(LampStand);
    
	// Methods
    //-------------------------------------------------------------------------
	virtual bool init();
    virtual bool initWithGravity(const CCPoint& gravity);


};


#endif