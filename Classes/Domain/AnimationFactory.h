#ifndef __ANIMATIONFACTORY_H__
#define __ANIMATIONFACTORY_H__
/*
Animation Factory
------------------------------------------------------------------
A source of animations to make things a little simpler.

Note: This needs to be reviewed to act as a "loader" for resources
    at some point...
*/
#include "cocos2d.h"

USING_NS_CC;	// Cocos2d Namespace

enum eAnim {
    animKittehSit,
    animKittehWalk,
    animKittehHurt,
    animKittehLook,
    animKittehAttack,
    animCrowFlap,
    animCrowSwoop,
    animCrowHurt
};

class AnimationFactory
{
private:
public:
	static CCAnimation* getAnim(eAnim anim);
};

#endif