#include "PropAttachPoint.h"
/************************************************************************

	PropAttachPoint

*************************************************************************/
#include "PropHazard.h"

// Constructor / Destructor
//-----------------------------------------------------------------------------
PropAttachPoint::PropAttachPoint()  { _position = ccp(0,0); _attached = NULL; }
PropAttachPoint::~PropAttachPoint() { _attached = NULL; }

PropAttachPoint* PropAttachPoint::create(const CCPoint& position) {
    PropAttachPoint* pRet = new PropAttachPoint();
    if(pRet && pRet->init(position)) {
        //pRet->autorelease();
        return pRet;
    }
    else { delete pRet; pRet = NULL; return NULL; }
}

// Properties
//-----------------------------------------------------------------------------
void            PropAttachPoint::setPosition(const CCPoint& position)   { this->_position = position; }
const CCPoint&  PropAttachPoint::getPosition()                          { return this->_position; }

PropHazard*     PropAttachPoint::getAttached()                          { return this->_attached; }
void            PropAttachPoint::setAttached(PropHazard* attached)      { this->_attached = attached; if(attached) attached->attach(this); }
void            PropAttachPoint::detach()                               { if(_attached) _attached->detach(); }

bool            PropAttachPoint::hasAttached()                          { if(this->_attached) return true; return false; }

// Methods
//-----------------------------------------------------------------------------
bool PropAttachPoint::init(const CCPoint& position) {
    this->setPosition(position);
    return true;
}