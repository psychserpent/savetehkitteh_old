#ifndef __KITTEH_H__
#define __KITTEH_H__
/*
KITTEH!!!!!!!!!!! .h
-------------------------------------------------------------------------------
Teh kitteh does stuff...

Detailed Description:
  Teh kitteh is basically the main player object in the scene.  It will wander
around and destroy the world with it's awesome cuteness.  Beware, the kitteh
is not indestructable.  It can be hit by other objects and critters.

*/
#include "cocos2d.h"
#include "AnimationFactory.h"
#include "PropBase.h"
#include <cmath>

// List of kitteh states
//-----------------------------------------------------------------------------
#define	KITTEH_SIT				     0 // Sitting.
#define KITTEH_WALK_LEFT		     1 // Walking to the left.
#define KITTEH_WALK_RIGHT		     2 // Walking to the right.
#define KITTEH_HURT				     3 // In hurt condition.
#define KITTEH_LOOKPROP              4 // Looking at a prop intently.
#define KITTEH_ATTACKPROP            5 // Attacking a prop.
#define KITTEH_WALK_TARGET           6 // Kitteh is walking to a target.

// List of special kitteh parameters
//-----------------------------------------------------------------------------
#define KITTEH_INVINCIBILITY       2.f // How long a kitteh is invincible.
#define KITTEH_SPEED              40.f // How fast the kitteh can move.
#define KITTEH_MAX_WALK_TIME       4.f // Max duration a kitteh will wander.
#define KITTEH_MAX_SIT_TIME        4.f // Max duration a kitteh will sit.
#define KITTEH_HURT_JUMP          0.3f // Max duration of kitteh flight time.
#define KITTEH_MAX_LOOK_DELAY     1.0f // Max duration of the kitteh looking.
#define KITTEH_TARGET_EXPIRE      20.f // How long til the kitteh will retarget.
#define KITTEH_MAX_TARGET_RANGE  100.f // How close the kitteh has to be within range.
#define KITTEH_ATTACK_CHANCE     1.00f // Chance the kitteh will attack prop.

// Minor tweak
#define KITTEH_FACING_CORRECTION  -1.f // Corrects the facing of the kitteh.

USING_NS_CC;	// Cocos2d Namespace

// Teh Kitteh
class Kitteh : public CCSprite
{
private:
    // Variables
    //--------------------------------------------
    //CCPoint _facing;        // Direction the kitteh is facing.  It should only
				//			// ever be horizontal only.
    CCPoint _velocity;      // The movement of the kitteh.

    PropBase* _target;      // Teh kitteh's current target.
    PropBase* _oldTarget;   // Teh kitteh's former target.

	float _time;			// How long the kitteh has been alive.
	int _state;				// Kitteh's current state.

    float _nextThought;     // When the kitteh's next thought will be.
    float _invincibility;   // How long the kitteh is invincible.
    float _targetExpire;    // How long until the old target expires.

    // Animations
    //--------------------------------------------
	CCAction* _walk;		// Walking animation
	CCAction* _sit;		    // Sitting animation
	CCAction* _hurt;        // Hurt animation
    CCAction* _look;        // Looking animation
    CCFiniteTimeAction* _attack;      // Attack animation

	int action;	// What the kitteh is doing.

    // Methods
    //--------------------------------------------
    void hurt();            // Kitteh hurt logic.
    void recover();         // End of the hurt logic.
protected:
    // Methods
    //--------------------------------------------
    void move(float dt);            // Mover method.
public:
	// Constructor
    //--------------------------------------------
	Kitteh(void);							// Kitteh Constructor
	~Kitteh(void);							// Kitteh Destructor (Noes!) :c
	CREATE_FUNC(Kitteh);					
	virtual bool init();					// Initializer (makes the kitteh special)
    // Properties
    //--------------------------------------------
	//void        setFacing(const CCPoint& facing);
	float           getFacing();

    void            faceLeft();                 // Makes the kitteh face left
    void            faceRight();                // Makes the kitteh face right

    void            setVelocity(const CCPoint& velocity);
    const CCPoint&  getVelocity();

	void            setState(int state);	// Sets the current state
	int             getState();				// gets the current state

    void            setTarget(PropBase* target);
    PropBase*       getTarget();
    bool            hasTarget();

    void            stepTime(float dt);
    float           getTime();

    const CCPoint&  getFoot();   // Gets the bottom of the kitteh sprite.
    void            setFoot(const CCPoint& foot); // Sets kitteh position based on foot.

    void            setNextThought(float dur); // Sets how long the next thought will take.
    void            setInvincibility(float dur); // Sets how long the kitteh is invincible for.

    bool            isInvincible(); //Determines if the kitteh is invincible or not.

    // Methods
    //--------------------------------------------
    void think();               // Calls the kitteh's next thought pattern.
    void collide(CCSprite* collider);  // Enemy/prop collides with kitteh.
    void lookAt(PropBase* prop); // Kitteh looks at a prop.
    void attack(PropBase* prop); // Kitteh attacks the prop.
    void attackComplete();       // Conclusion of the attack

    void faceTarget();           // Kitteh faces the direction of it's target.
    void walkTarget();           // Kitteh walks towards target.
    void expireTarget();         // Removes the kitteh's current target.

	virtual void update(float dt);			// Update schedule

};


#endif