#include "LampShade.h"
/************************************************************************

	Lamp Shade

*************************************************************************/

// Constructor / Destructor
//-----------------------------------------------------------------------------
LampShade::LampShade()  { }
LampShade::~LampShade() { }

bool LampShade::init() {
	if(!PropHazard::init()) return false;

	CCSpriteFrameCache* cache = CCSpriteFrameCache::sharedSpriteFrameCache();
	this->setDisplayFrame(cache->spriteFrameByName("prop0003.png"));
	this->setScale(2.f);

	return true;
}

bool LampShade::initWithGravity(const CCPoint& gravity) {
	if(!PropHazard::initWithGravity(gravity)) return false;
	
	CCSpriteFrameCache* cache = CCSpriteFrameCache::sharedSpriteFrameCache();
	this->setDisplayFrame(cache->spriteFrameByName("prop0003.png"));
	this->setScale(2.f);
	
	return true;
}
