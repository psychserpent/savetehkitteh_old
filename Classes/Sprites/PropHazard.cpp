#include "PropHazard.h"
/******************************************************************************

	Prop Hazard

******************************************************************************/
#include "PropAttachPoint.h"

// Constructor / Destructor
//-----------------------------------------------------------------------------
PropHazard::PropHazard()  { _tilt = 0; _baseTag = 0; _grabbed = false; this->setTag(TAG_PROPHAZARD); }
PropHazard::~PropHazard() { }

// Properties
//-----------------------------------------------------------------------------
int             PropHazard::getBaseTag()                         { return _baseTag; }
void            PropHazard::setBaseTag(int nTag)                 { _baseTag = nTag; }

bool			PropHazard::isGrabbed()							 { return _grabbed; }

const CCPoint&  PropHazard::getGravity()                         { return _gravity; }
void            PropHazard::setGravity(const CCPoint& gravity)   { _gravity = gravity; }

const CCPoint&  PropHazard::getVelocity()                        { return _velocity; }
void            PropHazard::setVelocity(const CCPoint& velocity) { _velocity = velocity; }

void            PropHazard::setTilt(float tilt)                  { _tilt = tilt; }

// Methods
//-----------------------------------------------------------------------------
bool            PropHazard::isAttached()                        { return _attachedTo; }

void PropHazard::attach(PropAttachPoint* atPoint) { 
	_attachedTo = atPoint; 
	if(atPoint) {
		this->unscheduleUpdate(); 
		CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
		this->setRotation(0);
		this->setVelocity(ccp(0, 0));
		this->_grabbed = false;
	}

}

void PropHazard::detach() { 
    if(_attachedTo) _attachedTo->setAttached(NULL);
    _attachedTo = NULL;
    this->scheduleUpdate(); 
	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, 0, true);
}


// Initialization
bool PropHazard::init() {
	return CCSprite::init();
    this->setBaseTag(TAG_PROPHAZARD);
}

// Initialization with gravity
bool PropHazard::initWithGravity(const CCPoint& gravity) {
	if(!CCSprite::init()) 
		return false; 
	this->_gravity = gravity; 
    this->setBaseTag(TAG_PROPHAZARD);
	return true;
}

// Moves the prophazard by velocity after applying accelleration
void PropHazard::move(float dt) {
	this->_velocity = this->_velocity + this->_gravity * dt;
	this->setPosition(this->getPosition() + this->_velocity * dt);
    this->setRotation(this->getRotation() + this->_tilt * dt);
}

// Destroys the prop hazard.
void PropHazard::destroy() {
	
	CCTexture2D* texture = this->getTexture();

	// Create the base rect which is the top left rect.
	CCRect rect00 = CCRect(this->getTextureRect());

	rect00.size.width = rect00.size.width * 0.5f;
	rect00.size.height = rect00.size.height * 0.5f;

	// Create the other rects which are slightly adjusted.
	CCRect rect10 = CCRect(rect00);		// Top Right rect
	rect10.origin.x += rect10.size.width;

	CCRect rect01 = CCRect(rect00);		// Bottom left rect
	rect01.origin.y += rect01.size.height;

	CCRect rect11 = CCRect(rect00);		// Bottom right rect
	rect11.origin.y += rect11.size.height;
	rect11.origin.x += rect11.size.width;


	CCSpriteFrame* frame00 = CCSpriteFrame::createWithTexture(texture, rect00);
	CCSpriteFrame* frame01 = CCSpriteFrame::createWithTexture(texture, rect01);
	CCSpriteFrame* frame10 = CCSpriteFrame::createWithTexture(texture, rect10);
	CCSpriteFrame* frame11 = CCSpriteFrame::createWithTexture(texture, rect11);

	// Top left debris.
	Debris* topLeft = Debris::create(this->getGravity(), frame00);
	topLeft->setScale(this->getScale());
	topLeft->setAnchorPoint(ccp(1.f, 0.f));
	topLeft->setRotation(this->getRotation());
	topLeft->setPosition(ccp(this->getPositionX(), this->getPositionY()));
	topLeft->setVelocity(ccp(-15.f, 0.f));
	topLeft->setTilt(-30.f);
	this->getParent()->addChild(topLeft, 1);

	// Top right debris.
	Debris* topRight = Debris::create(this->getGravity(), frame10);
	topRight->setScale(this->getScale());
	topRight->setAnchorPoint(ccp(0.f, 0.f));
	topRight->setRotation(this->getRotation());
	topRight->setPosition(ccp(this->getPositionX(), this->getPositionY()));
	topRight->setVelocity(ccp(15.f, 0.f));
	topRight->setTilt(30.f);
	this->getParent()->addChild(topRight, 1);
	
	// Bottom left debris.
	Debris* bottomLeft = Debris::create(this->getGravity(), frame01);
	bottomLeft->setScale(this->getScale());
	bottomLeft->setAnchorPoint(ccp(1.f, 1.f));
	bottomLeft->setRotation(this->getRotation());
	bottomLeft->setPosition(ccp(this->getPositionX(), this->getPositionY()));
	bottomLeft->setVelocity(ccp(-30.f, -20.f));
	bottomLeft->setTilt(-50.f);
	this->getParent()->addChild(bottomLeft, 1);
	
	// Bottom right debris.
	Debris* bottomRight = Debris::create(this->getGravity(), frame11);
	bottomRight->setScale(this->getScale());
	bottomRight->setAnchorPoint(ccp(0.f, 1.f));
	bottomRight->setRotation(this->getRotation());
	bottomRight->setPosition(ccp(this->getPositionX(), this->getPositionY()));
	bottomRight->setVelocity(ccp(30.f, -20.f));
	bottomRight->setTilt(50.f);
	this->getParent()->addChild(bottomRight, 1);

	this->removeFromParentAndCleanup(true);
}

void PropHazard::update(float dt)
{
    this->move(dt);
}


void PropHazard::onEnter() {
	
	CCSprite::onEnter();

}

void PropHazard::onExit() {
	
	CCSprite::onEnter();
}


bool PropHazard::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
    CCPoint tLoc = touch->getLocation();
    if(!this->boundingBox().containsPoint(tLoc))
        return false;

	this->unscheduleUpdate();
	this->setVelocity(ccp(0,0));
	this->_grabbed = true;
	this->setPosition(tLoc);

	return true;
}

void PropHazard::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	this->setPosition(touch->getLocation());
	this->setVelocity((touch->getLocation() - touch->getPreviousLocation()) * 40.f);
}

void PropHazard::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	this->_grabbed = false;
	this->scheduleUpdate();
}